console.log("helooooo :)")

//////////////////////////////////// Clock ////////////////////////////////////////

let divtime = document.getElementById("divtime");
function time()
{
    let date = new Date();
    let dd = date.getDate();
    let mm = date.getMonth();
    let yy = date.getFullYear();
    let hh = date.getHours();
    let min = date.getMinutes();
    let ss = date.getSeconds();

    let period = "AM";


    if(hh == 0)
    {
        hh = 12;
    }

    if(hh > 12)
    {
        hh = hh - 12;
        period = "PM";
    }

    console.log("Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+
        ":" +period)

        divtime.innerHTML = "Today's date: "+dd+
        "."+(mm+1)+
        "."+yy+
        " Time: "+hh+
        ":"+min+
        ":"+ss+ 
        ":" +period;
}

setInterval(time, 1000);

//////////////////////////////////// Game - vars ////////////////////////////////////////

const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

let gamerInput = new GamerInput("None");

let xpos = 0; //these are the position of the player, change them to make him spawn elsewhere
let ypos = 0;
let direction = 0; //this changes depending which way he faces, starts down then goes like sprite sheet, 1 = right, 2=left, 3 =up

let hollowImg = new Image();
hollowImg.src = "assets/hollow.png";

//////////////////////////////////// functions ////////////////////////////////////////

function GamerInput(input)
{
    this.action = input;
}

function gameLoop()
{
    update(); //update before drawing
    draw();
    window.requestAnimationFrame(gameLoop);    
}

function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height); //this goes first, as it needs to clear before drawing.   
    current = new Date().getTime();
    if (current - initial >= 100) {
        currentFrame = (currentFrame + 1) % frameCount;
        initial = current;
    }
    console.log(currentFrame);
    if (gamerInput.action !== "None")
    {
        context.drawImage(hollowImg, (128 * currentFrame), (127 * direction), 121, 127, xpos, ypos, 100, 100); 
    }
    else
    {
        context.drawImage(hollowImg, 0, (127 * direction), 121, 127, xpos, ypos, 100, 100); 
    }
}



function input(event) 
{

    if (event.type == "keydown") 
    {

        switch (event.keyCode) 
        {
            case 37:
                gamerInput = new GamerInput("Left");
                break;
            case 65:
                gamerInput = new GamerInput("Left");
                break;
            case 38:
                gamerInput = new GamerInput("Up");
                break; 
            case 87:
                gamerInput = new GamerInput("Up");
                break;
            case 39:
                gamerInput = new GamerInput("Right");
                break;
            case 68:
                gamerInput = new GamerInput("Right");
                break;
            case 40:
                gamerInput = new GamerInput("Down");
                break;
            case 83:
                gamerInput = new GamerInput("Down");
                break;
            case 32:
                console.log("boom");
                document.getElementById('boom').play();
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    else
    {
        gamerInput = new GamerInput("None");
    }
}

function playerMVMT()
{
    if (gamerInput.action == "Up")
    {
        ypos -= 5;
        direction = 3;
    }
    if (gamerInput.action == "Down")
    {
        ypos += 5;
        direction = 0;
    }
    if (gamerInput.action == "Left")
    {
        xpos -= 5;
        direction = 2;
    }
    if (gamerInput.action == "Right")
    {
        xpos += 5;
        direction = 1;
    }

}

//////////////////////////////////// animations ////////////////////////////////////////

const width = 121;
const height = 127;

let currentLoopIndex = 0;
let frameCount = 6; //amount of frames in sprite
let initial = new Date().getTime();
let current; 
let currentFrame = 0;


function update()
{
    playerMVMT();
}



window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown',input);
window.addEventListener('keyup',input);